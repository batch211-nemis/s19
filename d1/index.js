//console.log("Hello World");

//What are Conditional Statements?

//allows us to control the flow of our program
//allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise

let numA = -1;

//if statement
	//executes statement if a specified condition is true

	if(numA<0){
		console.log("Yes! It is less than zero.");
	};

	/*
		Syntax:
			if (condition) {
				statement
			}
	*/

	//the result of the expression added in the if's condition must result to true, else, the statement inside if() will not run
	console.log(numA<0); //results to true -- the if statement will run

	numA = 0;

	if(numA<0){
		console.log("Hello");
	};

	console.log(numA<0); //false

	let city = "New York";

	if (city === "New York"){
		console.log("Welcome to New York City!");
	};

	//else if clause

	/*
		-executes a statement if previous conditions are false and if the specified condition is true
		-the else-if clause is optional and can be added to capture additional conditions to change the flow of a program
	*/

	let numH = 1;

	if(numA<0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};

	numA = 1;

	if(numA>0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};


	city = "Tokyo";

	if (city === "New York"){
		console.log("Welcome to New York City!");
	} else if (city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!");
	};

	//else statement

	/*
		-executes a statement if all other conditions are false
		-the else statement is optional and can be added to capture any other result to change the flow of a program
	*/

	if (numA<0){
		console.log("Hello");
	} else if (numH === 0){
		console.log("World");
	} else {
		console.log("Again");
	}

	/*else{
		console.log("Will not run without an if"); //error
	}*/

	//if, else, and else if statements with functions

	let message = "No message.";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){
		if (windSpeed < 30){
			return 'Not a typhoon yet.';
		} else if (windSpeed <= 61){
			return 'Tropical depression detected.';
		} else if (windSpeed >=62 && windSpeed <= 88){
			return 'Tropical Storm detected.';
		} else if (windSpeed >=89 || windSpeed <= 117){
			return 'Severe Tropical Storm detected.';
		} else {
			return 'Typhoon detected.';
		}
    }

	message = determineTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe tropical storm detected."){
		console.warn(message);
	}

	//mini-activity1
	/*function oddOrEvenChecker(num){
		if (num % 2 === 0){
			alert(num + " is an even number");
		} else {
			alert(num + " is an odd number");
		}
	}
	oddOrEvenChecker(7);
*/
	//mini-activity2
	/*function ageChecker(age){
		if (age >= 18){
			alert(age + " is allowed to drink");
			return(true);
		} else {
			alert(age + " is underaged");
			return(false);
		}
	}

	let isAllowedToDrink = ageChecker(10);
	console.log(isAllowedToDrink);
*/
	//Truthy and Falsy

	/*
		-in JS a truthy value is a value that is considered true when encountered in Boolean context
		-values in are considered true unless defined otherwise
		-falsy values/exceptions for truth:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. Nan
	*/

	//Truthy Examples
	/*
		-if the result of the expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		-expressions are any unit of code that can be evaluated to a value
	*/

	if (" "){
		console.log('Truthy');
	}
	if ([1,2,3]){
		console.log('Truthy');
	}
	if(34){
		console.log('Truthy');
	}

	//Falsy examples

	if (false){
		console.log('falsy');
	}
	if(0){
		console.log('falsy');
	}
	if(undefined){
		console.log('falsy');
	}

	//Conditional (Ternary) Operator

	/*
		-the conditional (ternary) operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy

		- can be used as an alternative to an "if else" statement
		-ternary operators have an implicit "return" statement meaning that without the return keyword, the resulting expressions can be stored in a variable

		Syntax:
			(expression) ? ifTrue : ifFalse;
	*/

	//Single Statement Execution

	let ternaryResult = (1<18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);
	console.log(ternaryResult);

	//Multiple Statements Execution
	/*
		-a function maybe defined then used in a ternary opeartor
	*/

	let name;

	function isOfLegalAge(){
		name = "John";
		return 'You are of the legal age limit';
	}

	function isUnderAge() {
		name = "Jane"
		return "You are under the age limit";
	}

	let age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of ternary operator in function: " + legalAge + ', ' + name);

	//Switch Statement

	/*
		-evaluates an expression and matches the expression's value to a case clause

		.toLowerCase function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case.

		-the expression is the information used to match the value provided in the sitch cases
		-variables are commonly used as expressions to allow varying user input to be used when comparing with switch case value
		-break statement is used to terminate the current loop once a match has been found
	*/

	let day = prompt("What day of teh week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'indigo':
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day");
			break;
	}

	//mini-activity

	function determineBear(bearNumber){
		let bear;

		switch (bearNumber) {
		case 1:
			alert("Hi, I'm Amy!");
			break;
		case 2:
			alert("Hey, I'm Lulu!");
			break;
		case 3:
			alert("Hi, I'm Morgan!");
			break;
		default:
			bear = bearNumber + ' is out of bounds!';
			break;
		}
		return bear;
	}
	determineBear(3);

	//Try-Catch-Finally Statement
	/*
		- are commonly used for error handling
		-they are used to specify a response whenever an error is received
	*/

	function showIntensityAlert(windSpeed){
		try{ 
			alert(determineTyphoonIntensity(windSpeed))
		} catch (error) {
			console.log(typeof error);
			console.warn(error.message);
		} finally {
			alert('Intensity Updates will show new alert.');
		}
	}

	showIntensityAlert(56);